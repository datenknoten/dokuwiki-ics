<?php

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_LF')) define('DOKU_LF', "\n");
if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

require_once DOKU_PLUGIN.'syntax.php';

use Ulrichsg\Getopt\Getopt;
use Ulrichsg\Getopt\Option;
use Sabre\VObject;

class syntax_plugin_ics extends DokuWiki_Syntax_Plugin {
    /**
     * Check if a given option has been given, and remove it from the initial string
     * @param string $match The string match by the plugin
     * @param string $pattern The pattern which activate the option
     * @param $varAffected The variable which will memorise the option
     * @param $valIfFound the value affected to the previous variable if the option is found
     */
    private function _checkOption(&$match, $pattern, &$varAffected, $valIfFound){
        if ( preg_match($pattern, $match, $found) ){
            $varAffected = $valIfFound;
            $match = str_replace($found[0], '', $match);
        }
    } // _checkOption

    public function getType() {
        return 'substition';
    }

    public function getPType() {
        return 'block';
    }

    public function getSort() {
        return 0;
    }


    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('{ics[^>]*}',$mode,'plugin_ics');
    }


    public function handle($match, $state, $pos, &$handler){
        $match = trim(substr($match,4));
        $match = substr($match,0,strlen($match)-1);
        $from = new Option(null, "from", Getopt::REQUIRED_ARGUMENT);
        $from->setDefaultValue("now");
        $preview = new Option(null, "previewDays", Getopt::REQUIRED_ARGUMENT);
        $preview->setDefaultValue("14");
        $options = new Getopt([
            new Option(null, "url", Getopt::REQUIRED_ARGUMENT),
            $from,
            $preview,
        ]);
        $options->parse($match);

        //$this->_checkOption($match, "/-url/i", $return['url'], true);
        return $options->getOptions();
    }

    public function render($mode, &$renderer, $data) {
        global $conf;

        if($mode != 'xhtml') return false;

        $url = $data["url"];

        $days = $data["previewDays"];

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        $ics = (string)$res->getBody();

        $vcalendar = VObject\Reader::read(
            $ics
        );

        $events = [];
        
        $from = new DateTime();
        $from->setTime(0,0,0);

        $then = clone $from;
        $then->add(new DateInterval(sprintf("P%dD",$days)));
        $then->setTime(23,59,59);

        foreach ($vcalendar->VEVENT as $event) {
            $startdate = $event->DTSTART->getDateTime();
            if (!is_null($event->DTEND)) {
                $enddate = $event->DTEND->getDateTime();
            } else {
                $enddate = null;
            }

            if ($startdate < $from)
                continue;
            if ($enddate > $then)
                continue;
            $events[] = [
                "summary" => (string)$event->SUMMARY,
                "location" => (string)$event->LOCATION,
                "startdate" => $startdate->format("d.m.Y H:i"),
                "enddate" => (is_null($enddate) ? "" : $enddate->format("d.m.Y H:i")),
                "url" => (string)$event->URL,
                "description" => (string)$event->DESCRIPTION,
            ];
        }

        $template = <<<'EOD'
<ul>
{% for event in events %}
<li class="level1">
<div class="li">
<strong>{{ event.startdate }}</strong> <a class="urlextern" href="{{ event.url }}">{{ event.summary }}</a> — {{ event.description|replace({'\\n': "\n"})|nl2br }}
</div>
</li>
{% endfor %}
</ul>
EOD;

        $loader = new Twig_Loader_String();
        $environment = new Twig_Environment($loader, ['debug' => true]);
        $environment->addExtension(new Twig_Extension_Debug());
        $content = $environment->render($template,[ "events" => $events ]);


        $renderer->doc .= $content;
        return true;
    }
}
